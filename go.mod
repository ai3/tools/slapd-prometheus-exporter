module git.autistici.org/ai3/tools/slapd-prometheus-exporter

go 1.14

require (
	git.autistici.org/ai3/go-common v0.0.0-20230816213645-b3aa3fb514d6
	github.com/go-ldap/ldap/v3 v3.4.4
	github.com/prometheus/client_golang v1.12.2
)
