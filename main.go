package main

import (
	"context"
	"flag"
	"log"
	"net/http"
	"os"
	"strings"
	"time"

	ldaputil "git.autistici.org/ai3/go-common/ldap"
	"git.autistici.org/ai3/go-common/serverutil"
	"github.com/go-ldap/ldap/v3"
	"github.com/prometheus/client_golang/prometheus"
)

var (
	addr          = flag.String("addr", ":9389", "`address` to listen on")
	ldapURI       = flag.String("uri", "ldapi:///", "ldap server `uri`")
	baseDN        = flag.String("base", "", "base `DN` for contextCSN monitoring")
	scrapePeriod  = flag.Duration("interval", 10*time.Second, "scraping interval")
	scrapeTimeout = flag.Duration("timeout", 5*time.Second, "scraping timeout")
)

// Each LDAP query is a separate Prometheus metric. In order to
// support asynchronous polling, query objects hold their own copy of
// the data, and render it to Prometheus on demand.
type ldapQuery interface {
	Request() *ldap.SearchRequest
	Handle([]*ldap.Entry)
	Collect(chan<- prometheus.Metric)
	Describe(ch chan<- *prometheus.Desc)
}

// The LDAP metrics scraper runs queries periodically.
type ldapMetrics struct {
	pool   *ldaputil.ConnectionPool
	stopCh chan bool

	queries []ldapQuery
}

func newLDAPMetrics(uri, rootDN string) (*ldapMetrics, error) {
	pool, err := ldaputil.NewConnectionPool(uri, "", "", 2)
	if err != nil {
		return nil, err
	}

	queries := []ldapQuery{
		newOperationsQuery(),
		newStatisticsQuery(),
		newConnectionsQuery(),
		newThreadsQuery(),
		newWaitersQuery(),
	}
	if rootDN != "" {
		queries = append(queries, newBaseContextCSNQuery(rootDN))
	}

	for _, query := range queries {
		if err := prometheus.Register(query); err != nil {
			return nil, err
		}
	}

	m := &ldapMetrics{
		pool:    pool,
		stopCh:  make(chan bool),
		queries: queries,
	}
	go m.asynchronousScraper()
	return m, nil
}

func (m *ldapMetrics) Close() {
	close(m.stopCh)
}

func (m *ldapMetrics) asynchronousScraper() {
	m.scrape()

	tick := time.NewTicker(*scrapePeriod)
	defer tick.Stop()
	for {
		select {
		case <-tick.C:
			m.scrape()
		case <-m.stopCh:
			return
		}
	}
}

func (m *ldapMetrics) scrape() {
	ctx, cancel := context.WithTimeout(context.Background(), *scrapeTimeout)
	defer cancel()

	start := time.Now()
	err := m.runQueries(ctx)
	scrapeTime.Set(time.Since(start).Seconds())

	if err != nil {
		// status down
		log.Printf("scrape error: %v", err)
		slapdUp.Set(0)
	} else {
		// status ok
		slapdUp.Set(1)
	}
}

// Run queries sequentially.
func (m *ldapMetrics) runQueries(ctx context.Context) error {
	for _, query := range m.queries {
		result, err := m.pool.Search(ctx, query.Request())
		if err != nil {
			return err
		}
		query.Handle(result.Entries)
	}
	return nil
}

var (
	slapdUp = prometheus.NewGauge(
		prometheus.GaugeOpts{
			Name: "slapd_up",
			Help: "Is slapd up and answering requests.",
		},
	)
	scrapeTime = prometheus.NewGauge(
		prometheus.GaugeOpts{
			Name: "exporter_scrape_time",
			Help: "Duration of LDAP queries.",
		},
	)
)

func init() {
	prometheus.MustRegister(slapdUp, scrapeTime)
}

func setFlagDefaultsFromEnv() {
	flag.VisitAll(func(f *flag.Flag) {
		envVar := strings.ToUpper(strings.Replace(f.Name, "-", "_", -1))
		if value := os.Getenv(envVar); value != "" {
			f.DefValue = value
			f.Value.Set(value)
		}
	})
}

func main() {
	log.SetFlags(0)
	setFlagDefaultsFromEnv()
	flag.Parse()

	m, err := newLDAPMetrics(*ldapURI, *baseDN)
	if err != nil {
		log.Fatal(err)
	}
	defer m.Close()

	log.Printf("slapd-prometheus-exporter starting on %s", *addr)
	err = serverutil.Serve(
		http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			http.NotFound(w, r)
		}),
		nil,
		*addr,
	)
	if err != nil {
		log.Fatal(err)
	}
}
