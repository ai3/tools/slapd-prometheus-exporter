package main

import (
	"strconv"
	"strings"
	"sync"

	"github.com/go-ldap/ldap/v3"
	"github.com/prometheus/client_golang/prometheus"
)

// Time limit for all our LDAP queries (in seconds).
var defaultTimeLimit = 5

// Base type for LDAP query exporters. Holds the metric descriptor,
// and provides a mutex for handle/collect synchronization.
type queryBase struct {
	sync.Mutex
	desc []*prometheus.Desc
}

func newQueryBase(desc ...*prometheus.Desc) *queryBase {
	return &queryBase{desc: desc}
}

func (q *queryBase) Describe(ch chan<- *prometheus.Desc) {
	for _, d := range q.desc {
		ch <- d
	}
}

// Operation counters (initiated, completed by type).
type operationsQuery struct {
	*queryBase
	opInitiated map[string]int64
	opCompleted map[string]int64
}

func newOperationsQuery() *operationsQuery {
	return &operationsQuery{
		queryBase: newQueryBase(prometheus.NewDesc(
			"slapd_operations",
			"Slapd operations counter.",
			[]string{"state", "name"},
			nil,
		)),
	}
}

func (q *operationsQuery) Request() *ldap.SearchRequest {
	return &ldap.SearchRequest{
		BaseDN:       "cn=Operations,cn=Monitor",
		Scope:        ldap.ScopeSingleLevel,
		DerefAliases: ldap.NeverDerefAliases,
		Filter:       "(objectClass=monitorOperation)",
		Attributes:   []string{"cn", "monitorOpInitiated", "monitorOpCompleted"},
		TimeLimit:    defaultTimeLimit,
	}
}

func (q *operationsQuery) Handle(entries []*ldap.Entry) {
	q.Lock()
	q.opInitiated = make(map[string]int64)
	q.opCompleted = make(map[string]int64)
	for _, entry := range entries {
		cn := entry.GetAttributeValue("cn")
		if s := entry.GetAttributeValue("monitorOpInitiated"); s != "" {
			q.opInitiated[cn] = toInt64(s)
		}
		if s := entry.GetAttributeValue("monitorOpCompleted"); s != "" {
			q.opCompleted[cn] = toInt64(s)
		}
	}
	q.Unlock()
}

func (q *operationsQuery) Collect(ch chan<- prometheus.Metric) {
	q.Lock()
	for name, value := range q.opInitiated {
		ch <- prometheus.MustNewConstMetric(
			q.desc[0],
			prometheus.CounterValue,
			float64(value),
			"initiated",
			name,
		)
	}
	for name, value := range q.opCompleted {
		ch <- prometheus.MustNewConstMetric(
			q.desc[0],
			prometheus.CounterValue,
			float64(value),
			"completed",
			name,
		)
	}
	q.Unlock()
}

// Generic type for monitorCounterObject counters. It will use the CN
// attribute to set the 'name' label.
type counterObjectQuery struct {
	*queryBase
	counters map[string]int64
	baseDN   string
}

func newCounterObjectQuery(metricName, metricHelp, baseDN string) *counterObjectQuery {
	return &counterObjectQuery{
		queryBase: newQueryBase(prometheus.NewDesc(
			metricName,
			metricHelp,
			[]string{"name"},
			nil,
		)),
		baseDN: baseDN,
	}
}

func (q *counterObjectQuery) Request() *ldap.SearchRequest {
	return &ldap.SearchRequest{
		BaseDN:       q.baseDN,
		Scope:        ldap.ScopeSingleLevel,
		DerefAliases: ldap.NeverDerefAliases,
		Filter:       "(objectClass=monitorCounterObject)",
		Attributes:   []string{"cn", "monitorCounter"},
		TimeLimit:    defaultTimeLimit,
	}
}

func (q *counterObjectQuery) Handle(entries []*ldap.Entry) {
	q.Lock()
	q.counters = make(map[string]int64)
	for _, entry := range entries {
		cn := entry.GetAttributeValue("cn")
		if s := entry.GetAttributeValue("monitorCounter"); s != "" {
			q.counters[cn] = toInt64(s)
		}
	}
	q.Unlock()
}

func (q *counterObjectQuery) Collect(ch chan<- prometheus.Metric) {
	q.Lock()
	for name, value := range q.counters {
		ch <- prometheus.MustNewConstMetric(
			q.desc[0],
			prometheus.CounterValue,
			float64(value),
			name,
		)
	}
	q.Unlock()
}

// Generic slapd statistics.
func newStatisticsQuery() *counterObjectQuery {
	return newCounterObjectQuery(
		"slapd_stats",
		"Slapd statistics.",
		"cn=Statistics,cn=Monitor",
	)
}

// Waiters statistics.
func newWaitersQuery() *counterObjectQuery {
	return newCounterObjectQuery(
		"slapd_waiters",
		"Slapd waiter statistics.",
		"cn=Waiters,cn=Monitor",
	)
}

// Thread-related metrics (thread counts by state).
type threadsQuery struct {
	*queryBase
	threads map[string]int64
}

func newThreadsQuery() *threadsQuery {
	return &threadsQuery{
		queryBase: newQueryBase(prometheus.NewDesc(
			"slapd_threads",
			"Slapd thread statistics.",
			[]string{"state"},
			nil,
		)),
	}
}

func (q *threadsQuery) Request() *ldap.SearchRequest {
	return &ldap.SearchRequest{
		BaseDN:       "cn=Threads,cn=Monitor",
		Scope:        ldap.ScopeSingleLevel,
		DerefAliases: ldap.NeverDerefAliases,
		Filter:       "(&(!(|(cn=Runqueue)(cn=Tasklist)))(objectClass=monitoredObject))",
		Attributes:   []string{"cn", "monitoredInfo"},
		TimeLimit:    defaultTimeLimit,
	}
}

func (q *threadsQuery) Handle(entries []*ldap.Entry) {
	q.Lock()
	q.threads = make(map[string]int64)
	for _, entry := range entries {
		cn := entry.GetAttributeValue("cn")
		if s := entry.GetAttributeValue("monitoredInfo"); s != "" {
			q.threads[cn] = toInt64(s)
		}
	}
	q.Unlock()
}

func (q *threadsQuery) Collect(ch chan<- prometheus.Metric) {
	q.Lock()
	for name, value := range q.threads {
		ch <- prometheus.MustNewConstMetric(
			q.desc[0],
			prometheus.GaugeValue,
			float64(value),
			name,
		)
	}
	q.Unlock()
}

// Monitor the contextCSN attribute of the database root.
type baseContextCSNQuery struct {
	*queryBase
	csn    float64
	rootDN string
}

func newBaseContextCSNQuery(rootDN string) *baseContextCSNQuery {
	return &baseContextCSNQuery{
		queryBase: newQueryBase(prometheus.NewDesc(
			"slapd_context_csn",
			"ContextCSN timestamp of the base object.",
			nil,
			nil,
		)),
		rootDN: rootDN,
	}
}

func (q *baseContextCSNQuery) Request() *ldap.SearchRequest {
	return &ldap.SearchRequest{
		BaseDN:       q.rootDN,
		Scope:        ldap.ScopeBaseObject,
		DerefAliases: ldap.NeverDerefAliases,
		Filter:       "(objectClass=*)",
		Attributes:   []string{"contextCSN"},
		TimeLimit:    defaultTimeLimit,
	}
}

func csnToFloat(csn string) float64 {
	if r := strings.IndexByte(csn, 'Z'); r >= 0 {
		csn = csn[:r]
	}
	f, _ := strconv.ParseFloat(csn, 64) // nolint
	return f
}

func (q *baseContextCSNQuery) Handle(entries []*ldap.Entry) {
	if len(entries) < 1 {
		return
	}
	entry := entries[0]

	// Pick the maximum value for the contextCSN, in case we have
	// multiple values.
	var csnValue float64
	for _, csn := range entry.GetAttributeValues("contextCSN") {
		if v := csnToFloat(csn); v > csnValue {
			csnValue = v
		}
	}
	q.Lock()
	q.csn = csnValue
	q.Unlock()
}

func (q *baseContextCSNQuery) Collect(ch chan<- prometheus.Metric) {
	q.Lock()
	ch <- prometheus.MustNewConstMetric(
		q.desc[0],
		prometheus.GaugeValue,
		q.csn,
	)
	q.Unlock()
}

// Connection counters. This would be a simple counterObjectQuery,
// except that some of the values are gauges and some are counters, so
// we're better off just creating dedicated metrics for each.
type connectionsQuery struct {
	*queryBase
	current, total int64
}

func newConnectionsQuery() *connectionsQuery {
	return &connectionsQuery{
		queryBase: newQueryBase(
			prometheus.NewDesc(
				"slapd_connections_current",
				"Currently active connections to slapd.",
				nil,
				nil,
			),
			prometheus.NewDesc(
				"slapd_connections_total",
				"Total connections to slapd.",
				nil,
				nil,
			),
		),
	}
}

func (q *connectionsQuery) Request() *ldap.SearchRequest {
	return &ldap.SearchRequest{
		BaseDN:       "cn=Connections,cn=Monitor",
		Scope:        ldap.ScopeSingleLevel,
		DerefAliases: ldap.NeverDerefAliases,
		Filter:       "(objectClass=monitorCounterObject)",
		Attributes:   []string{"cn", "monitorCounter"},
		TimeLimit:    defaultTimeLimit,
	}
}

func (q *connectionsQuery) Handle(entries []*ldap.Entry) {
	q.Lock()
	for _, entry := range entries {
		cn := entry.GetAttributeValue("cn")
		value := toInt64(entry.GetAttributeValue("monitorCounter"))
		switch cn {
		case "Current":
			q.current = value
		case "Total":
			q.total = value
		}
	}
	q.Unlock()
}

func (q *connectionsQuery) Collect(ch chan<- prometheus.Metric) {
	q.Lock()
	current := q.current
	total := q.total
	q.Unlock()

	ch <- prometheus.MustNewConstMetric(
		q.desc[0],
		prometheus.GaugeValue,
		float64(current),
	)
	ch <- prometheus.MustNewConstMetric(
		q.desc[1],
		prometheus.CounterValue,
		float64(total),
	)
}

func toInt64(s string) int64 {
	i, _ := strconv.ParseInt(s, 10, 64) // nolint
	return i
}
